# ADAPBEEN
## Adaptatible Automata Framework

## Mais detalhes
Um artigo sobre o trabalho foi publicado no WTA (Workshop de Tecnologia Adaptativa) em 2018, você pode ter acesso ao artigo através do endereço: http://lta.poli.usp.br/lta/publicacoes/artigos/2018/memorias-do-wta-2018 .

## Informações para citação
> Reinaldo Felipe Soares Araujo, João Augusto Felberg Jacobsen, Willians Magalhães Primo, Reginaldo Inojosa da Silva Filho. ADAPBEEN - Adaptible Automata Framework. Em: Memórias do XII Workshop de Tecnologia Adaptativa - WTA 2018. EPUSP, São Paulo. ISBN: 978-85-86686-98-6, pp. 11-16. 1 e 2 de Fevereiro, 2018.

## Resumo

Neste trabalho apresentamos o ADAPBEEN, um framework que implementa o modelo algébrico dos autômatos adaptativos de primeira e segunda ordem. Nosso framework permite que sejam construídas aplicações que utilizem a tecnologia adaptativa, ou seja, programas que mudam seu comportamento em função dos inputs recebidos. Para tanto, o ADAPBEEN foi construído de maneira modular. Para obter uma melhor performance, ele foi desenvolvido em C++ e de forma a permitir que o código seja portável.

## Fundamentação Teórica

Os autômatos adaptativos possuem a capacidade de modificar a própria estrutura em tempo de execução, com isso, estados e transições podem ser acrescentados ou removidos. Essas alterações estruturais dependem da entrada, do estado atual do autômato e das transições associadas ao mesmo que, caso forem adaptativas, podem alterar a topologia do autômato.
Em função desse comportamento, o poder computacional o autômato adaptativo é equivalente ao de uma máquina de Turing [Rocha and Neto 2000], capaz de reconhecer, inclusive, linguagens livres de contexto, ou seja: embora o autômato adaptativo seja dotado de grande poder computacional, o mesmo  apresenta as mesmas restrições da máquina de Turing em relação a incomputabilidade [Queiroz 2010]. Durante os últimos anos, a tecnologia adaptativa tem se desenvolvido em uma série de aplicações. Os autômatos adaptativos, uma das facetas da tecnologia adaptativa, consiste em uma solução computacional para problemas complexos. 

O modelo escolhido para implementar o ADAPBEEN foi o da formulação algébrica para o Autômato  Adaptativo de Segunda Ordem [Silva Filho 2011],  que tem uma forte conexão com o aprendizado indutivo no limite. O formalismo do Autômato  Adaptativo de Segunda Ordem é desenvolvido sobre o modelo dos autômatos finitos de Primeira Ordem, [Silva Filho 2011], ou seja: enquanto o Autômato  Adaptativo de Primeira Ordem muda as regras do dispositivo subjacente (autômato finito não-determinístico), o Autômato  Adaptativo de Segunda Ordem muda funções adaptativas do Autômato  Adaptativo de Primeira Ordem.
Assim, as regras adaptativas (modeladas como transformações) constituídas de ações básicas de inserção e remoção de transições em um autômato finito não-determinístico, são, elas próprias, sujeitas à regras de segunda ordem que inserem ou removem regras dos Autômatos Adaptativos de Primeira Ordem, assim, a escolha do nome "Autômatos Adaptativos de Primeira Ordem" foi feita para levar à descrição de uma próxima ordem que tem, como dispositivo subjacente, um Autômato Adaptativo de Primeira Ordem.

## Exemplo de implementação de Autômato Finito de Primeira Ordem que reconhece L = a^n b^n | n >= 1

```sh
#include <string>
#include <iostream>
#include <map>
#include "AASO.h"
using namespace std;
int main() {
	atribAlphabet Alfabeto;
	atribState Estados;
	atribTransition mapTransicoes;
	atribState estadosIniciais;
	atribState estadoFinais;
	Alfabeto.insert(make_pair("a", 0));
	Alfabeto.insert(make_pair("b", 1));
	Estados.insert(make_pair(state("q1"), 0));
	Estados.insert(make_pair(state("q2"), 1));
	Estados.insert(make_pair(state("q3"), 2));
	mapTransicoes.insert(make_pair(0, transition(state("q1"), "a", state("q2’"))));
	mapTransicoes.insert(make_pair(1, transition(state("q2"), "a", state("q2’"))));
	mapTransicoes.insert(make_pair(2, transition(state("q2"), "b", state("q3’"))));
	estadosIniciais.insert(make_pair(state("q1"), 0));
	estadoFinais.insert(make_pair(state("q3"), 2));
	//map <int, transition>
	atribTransition _pro;
	atribTransition _for;
	positiveSequence transPro2;
	negativeSequence transFor2;
	atribPairFO conjuntodeComportamentosPO;
	atribPairFO auxPO;
	_pro.insert(make_pair(_pro.size(), transition(state("q3", "b", 0), "b", state("q3"))));
	_for.insert(make_pair(_for.size(), transition(state("q3", "b", 0), "b", state("qs"))));
	_for.insert(make_pair(_for.size(), transition(state("qs"), "b", state("q3"))));
	transPro2 = positiveSequence(_pro);
	transFor2 = negativeSequence(_for);
	pairFO novoPO(transPro2, transFor2);
	//pairFO novoPO;
	pair < int, pairFO > parAux(1, novoPO);
	pair < int, pairFO > F1(1, novoPO);
	novoPO = pairFO(transPro2, transFor2);
	conjuntodeComportamentosPO.insert(parAux);
	AAFO teste("AAFO", Alfabeto, Estados, estadosIniciais, estadoFinais, mapTransicoes, conjuntodeComportamentosPO);
	string entrada;
	while (cin >> entrada) {
		if (teste.output(entrada)) {
			cout << "Aceita" << endl;
		}
		else {
			cout << "Nao aceita" << endl;
		}
	}
}
```
## Referências

Rocha, R. L. A. and Neto, J. J. (2000). Autômato adaptativo, limites e complexidade em
comparação com máquina de turing. In Proceedings of the second Congress of Logic
Applied to Technology–LAPTEC, pages 33–48.


Silva Filho, R. I. d. (2011). Uma nova formulação algébrica para o autômato finito
adaptativo de segunda ordem aplicada a um modelo de inferência indutiva. PhD thesis,
Universidade de São Paulo

Queiroz, D. (2010). Uma definição simplificada para o estudo das propriedades dos autômatos finitos adaptativos. In Quarto Workshop de Tecnologia Adaptativa.
