#include "transition.h"

transition::transition(state origin, string symbol, state destiny) {
	this->origin = origin;
	this->symbol = symbol;
	this->destiny = destiny;
	shape += origin._name + " " + symbol + " " + destiny._name;
}

void transition::print() {
	cout << origin._name << " " << symbol << " " << destiny._name << endl;
}

state transition::Start() {
	return origin;
}

state transition::getOrigin() {
	return origin;
}

state transition::Fin() {
	return destiny;
}


state transition::getDestiny() {
	return destiny;
}

string transition::getSymbol() {
	return symbol;
}

void transition::setOrigin(string origin) {
	this->origin._name = origin;
}

void transition::setDestiny(string destiny) {
	this->destiny._name = destiny;
}

void transition::setSymbol(string symbol) {
	this->symbol = symbol;
}

bool transition::operator == (const transition &b) const {
	return b.shape == this->shape;
}

bool transition::operator < (const state &b) const {
	return b._name < this->shape;
}

bool transition::operator == (string b) {
	return b == this->shape;
}