#include "strings.h"

//AASO
mErrorRemSO = "We couldn't find a pair with id: ",
mCreateAutomataSO1 = "The automata without setoftest and setoftestSO was created.",
mCreateAutomataSO2 = "The automata without setoftestSO was created.",
mCreateAutomataSO3 = "Adaptible Automata was created.",
mInsPairSO = "Adaptible SO Pair was created.",
mRemPairSO = "Adaptible SO Pair was removed.";
/**/
//AAFO
mCreateAutomataFO1 = "Adaptible Automaton First Order was created without set of tests.",
mCreateAutomataFO2 = "Adaptible Automaton First Order was created.",
mInsFO = "Pair First Order was inserted.",
mRemFO = "Pair First Order was removed.";
//AFN/AFD
mCreateAFN = "Nondeterministic finite automaton was created.",
mCreateAFD = "Finite Automata Deterministic was created.",
mInsState = "State was inserted.",
mRemState = "State was removed.",
mInsTransition = "Transition was inserted.",
mRemTransition = "Transiton was removed.",
mRemSymbol = "Symbol was removed.",
mInsSymbol = "Symbol was inserted";
		
			

