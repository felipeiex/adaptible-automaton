#pragma once
#include "automaton.h"
#include "DFA.h"
//Nondeterministic Finite Automaton
/***
A classe NFA abreviação para Nondeterministic Finite Automaton (Autômatos Finitos Não-determinísticos) é usada como base para os autômatos adaptativos de primeira
e segunda ordem. Esta classe contem os métodos output, toAFNDG e toDFA.
O método output é utilizado para saber se a palavra parametrizada pertence ou não ao autômato, ou seja, se o retorno for verdadeiro a palavra então pertence ao
autômato e se for falso então a palavra não pertence ao autômato. 
O método toAFNDG transforma o autômato em um autômato generalizado que é um autômato que possui apenas um estado final.
O método toDFA retorna um autômato finito determinístico equivalente.

**/
class NFA :
	public automaton
{
private:
	atribState makefinalstates(atribState &statesDFA);
	atribState toDeterministic(atribTransition & transitionsTemp);
	void removeSymbVazio(atribTransition &_transitions);

public:
	NFA(string _name, atribAlphabet alphabet, atribState _state, atribState _initialStates, atribState _finalStates,
		atribTransition _transitions):automaton(_name, alphabet, _state, _initialStates, _finalStates, _transitions) {
		cout << mCreateAFN << endl;
	}
	NFA() {}
	bool output(string word, state current);
	bool output(string word);
	void toAFNDG();
	DFA toDFA();
};


