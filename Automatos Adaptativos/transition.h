#include "state.h"
#include <iostream>
#pragma once

/***
A classe transition possui os atributos origin, destiny, symbol e shape que são os componentes básicos de uma transição. Um autômato, geralmente, é composto em sua
estrutura por estados e transições, sendo essa classe responsável pelas transições do autômato.

As trasições são elementos básicos de um autômato que informa para qual estado o autômato deve ir caso esteja em um determinado estado e receba 
um determinado símbolo como entrada. Nesse modelo as transições podem ser criadas através da classe transition que possui os atributos origin, 
destiny, symbol e shape. Além dos geters se seters usuais do paradigma orientado a objeto a classe traz também os métodos Start e Fin que são
utilizados no modelo inspirado.

*/
class transition
{
public:
	//Attributes
	state origin, destiny;
	string symbol;
	string shape;

	//Methods
	transition(state origin, string symbol, state destiny);
	void print();
	state Start();
	state getOrigin();
	state Fin();
	state getDestiny();
	string getSymbol();
	void setOrigin(string origin);
	void setDestiny(string destiny);
	void setSymbol(string symbol);
	bool operator == (const transition &b) const;
	bool operator < (const state &b) const;
	bool operator == (string b);

};

