#include "AASO.h"

AASO::AASO(string _name, atribAlphabet alphabet, atribState _state, atribState _initialStates, atribState __finalStates, atribTransition _transitions) : AAFO(_name, alphabet, _state, _initialStates, __finalStates, _transitions)
{
	cout << mCreateAutomataSO1 << endl;
}

AASO::AASO(string _name, atribAlphabet alphabet, atribState _state, atribState _initialStates, atribState __finalStates, atribTransition _transitions, atribPairFO setoftests) : AAFO(_name, alphabet, _state, _initialStates, __finalStates, _transitions, setoftests)
{
	cout << mCreateAutomataSO2 << endl;
}

AASO::AASO(string _name, atribAlphabet alphabet, atribState _state, atribState _initialStates, atribState __finalStates, atribTransition _transitions, atribPairFO setoftests, atribPairSO setoftestsSO) : AAFO(_name, alphabet, _state, _initialStates, __finalStates, _transitions, setoftests)
{
	this->setoftestsSO = setoftestsSO;
	cout << mCreateAutomataSO3 << endl;
}

void AASO::insSO(pair<int, pairSO> _new)
{
	setoftestsSO.insert(_new);
	cout << mInsPairSO << endl;
}

void AASO::remSO(int id)
{
	atribPairSO::iterator it = setoftestsSO.find(id);
	if (it != setoftestsSO.end()) {
		setoftestsSO.erase(it);
		cout << mRemPairSO << endl;
	}
	else {
		cout << mErrorRemSO << id << "." << endl;
	}
}

bool AASO::inferAutomata(string word, state current, AASO currentAutomata)
{
	currentAutomata.printTransitions();
	bool out = false;
	AASO nextAutomata = currentAutomata;
	

	atribState::iterator itFinal = _finalStates.begin();
	for (itFinal; itFinal != _finalStates.end(); itFinal++) {
		if (current._name == "q10")
	
		if (current == itFinal->first) {

			this->inferedAutomata = nextAutomata;
			this->update();

			return true;
		}

	}

	atribTransition mapTran = currentAutomata.Sch(current, 1);
	atribTransition::iterator it = mapTran.begin();

	for (it; it != mapTran.end(); it++) {
		if (it->second.getSymbol() == symEmpty) {		
			atribPairFO::iterator currentPAIR = currentAutomata.setoftests.find(it->first);
			if (currentPAIR != currentAutomata.setoftests.end()) {
				
				atribTransition::iterator itTrans;
				negativeSequence currentNegative = currentPAIR->second._for;
				positiveSequence currentPositive = currentPAIR->second._own;
				vector < pair<int, transition> > aux;

				itTrans = currentNegative.foreignTransition.begin();
								for (itTrans; itTrans != currentNegative.foreignTransition.end(); itTrans++) {
				
					transition currenttransition = itTrans->second;
					state origin = currenttransition.Start();
					
					if (origin.activated) { 										  

						if (origin._name == dynamicState) {
							string q = "q";
							char aux[10];
							q += myitoa().operation(currentAutomata._states.size()  );							
							origin = state(q);;
						}
						else {
							atribTransition itTrans2 = currentAutomata.Sch(origin, origin.symbol, origin.stateOrigin);
							origin = itTrans2.begin()->second.getOrigin(); 
						}
					}
					
					state destiny = currenttransition.getDestiny();


					if (destiny == dynamicState) {
						string q = "q";
						char aux[10];
						q += myitoa().operation(currentAutomata._states.size()  );
						destiny = state(q);
						nextAutomata.ins(destiny);
					}
					
					transition newTran(origin, currenttransition.getSymbol(), destiny);
					aux.push_back(make_pair(itTrans->first, newTran));


				}
				
				itTrans = currentPositive.ownTransition.begin();
				for (itTrans; itTrans != currentPositive.ownTransition.end(); itTrans++) {
					transition currentTran = itTrans->second;
					state origin = currentTran.Start(); 
					transition ownTransition(origin, currentTran.getSymbol(), currentTran.Fin()); 
					if (origin.activated) { 
						atribTransition mapAux = currentAutomata.Sch(origin, origin.symbol, origin.stateOrigin);
						origin = mapAux.begin()->second.getOrigin();
						ownTransition = transition(origin, currentTran.getSymbol(), currentTran.Fin());
					}

					nextAutomata.rem(ownTransition);

				}

				fore(i, aux.size()) {					
					nextAutomata.ins(aux[i]);
				}
			}

			atribPairSO::iterator itSO = currentAutomata.setoftestsSO.find(it->first);

			if (itSO != currentAutomata.setoftestsSO.end()) {
				flag = true;
				this->inferedAutomata = *this;
				pairSO auxSO = itSO->second;
				positiveSequenceSO seqP = auxSO._own;
				negativeSequenceSO seqN = auxSO._for;

				atribPairFO::iterator itPO;

				for (itPO = seqP.ownTransition.begin(); itPO != seqP.ownTransition.end(); itPO++) {					
					nextAutomata.remFO(itPO->first);
				}

				for (itPO = seqN.foreignTransition.begin(); itPO != seqN.foreignTransition.end(); itPO++) {					
					nextAutomata.insFO(*itPO);
				}
				
			}


			out |= inferAutomata(word, it->second.getDestiny(), nextAutomata);
			if (out) return true;
		}
	}

	if (word.empty()) {
		itFinal = _finalStates.begin();
		for (itFinal; itFinal != _finalStates.end(); itFinal++) {
			if (current == itFinal->first) {			
				return true;

			}
		}

	}
	else {
		string a = word.substr(0, 1);
		word.erase(0, 1);

		atribTransition mapTran = currentAutomata.Sch(current, a, 1);
		atribTransition::iterator it = mapTran.begin();
		for (it; it != mapTran.end(); it++) {
			pair <int, transition> tranAtual = *it;

			if (tranAtual.second.getSymbol() == a) {
				atribPairFO::iterator currentPAIR = currentAutomata.setoftests.find(tranAtual.first);
				if (currentPAIR != currentAutomata.setoftests.end()) {

					atribTransition::iterator itTrans;
					negativeSequence currentNegative = currentPAIR->second._for;
					positiveSequence currentPositive = currentPAIR->second._own;
					vector < pair<int, transition> > aux;

					itTrans = currentNegative.foreignTransition.begin();
					
					for (itTrans; itTrans != currentNegative.foreignTransition.end(); itTrans++) {
					
						transition currenttransition = itTrans->second; 
						state origin = currenttransition.Start();

						if (origin.activated) { 											  

							if (origin._name == dynamicState) {
								string q = "q";
								char aux[10];
								q += myitoa().operation(currentAutomata._states.size()  );
								origin = state(q);;
							}
							else {
								atribTransition itTrans2 = currentAutomata.Sch(origin, origin.symbol, origin.stateOrigin);
								origin = itTrans2.begin()->second.getOrigin(); //Atualiza origin
							}
						}						
						state destiny = currenttransition.getDestiny();


						if (destiny == dynamicState) {
							string q = "q";
							char aux[10];
							q += myitoa().operation(currentAutomata._states.size()  );
							destiny = state(q);
							nextAutomata.ins(destiny); //Adiciona o novo estado.
						}						
						transition newTran(origin, currenttransition.getSymbol(), destiny);
						aux.push_back(make_pair(itTrans->first, newTran));


					}
					
					itTrans = currentPositive.ownTransition.begin();

					for (itTrans; itTrans != currentPositive.ownTransition.end(); itTrans++) {						
						transition currentTran = itTrans->second;
						state origin = currentTran.Start(); // state est�tico
						transition ownTransition(origin, currentTran.getSymbol(), currentTran.Fin()); //remover transicao com estado est�tico
						if (origin.activated) { // Verifico se � dinamico
							atribTransition mapAux = currentAutomata.Sch(origin, origin.symbol, origin.stateOrigin);
							origin = mapAux.begin()->second.getOrigin();
							ownTransition = transition(origin, currentTran.getSymbol(), currentTran.Fin());
						}

						nextAutomata.rem(ownTransition);

					}

					fore(i, aux.size()) {						
						nextAutomata.ins(aux[i]);
					}
				}				

				atribPairSO::iterator itSO = currentAutomata.setoftestsSO.find(tranAtual.first);

				if (itSO != currentAutomata.setoftestsSO.end()) {				
					flag = true;
					pairSO auxSO = itSO->second;
					positiveSequenceSO seqP = auxSO._own;
					negativeSequenceSO seqN = auxSO._for;

					atribPairFO::iterator itPO;

					for (itPO = seqP.ownTransition.begin(); itPO != seqP.ownTransition.end(); itPO++) {						
						nextAutomata.remFO(itPO->first);
					}

					for (itPO = seqN.foreignTransition.begin(); itPO != seqN.foreignTransition.end(); itPO++) {						
						nextAutomata.insFO(*itPO);
					}					
				}

				out |= inferAutomata(word, it->second.getDestiny(), nextAutomata);
				if (out) {					
					return true;
				}
			}
		}
	}



	return out;
}

bool AASO::inferAutomata(string word)
{
	return this->inferAutomata(word, this->getInitialStates().begin()->first, *this);
}

void AASO::update()
{
	this->setAlphabet(inferedAutomata.getAlphabet());
	this->setFinalStates(inferedAutomata.getFinalStates());
	this->setInitialStates(inferedAutomata.getInitialStates());
	this->setSetOfTests(inferedAutomata.getSetOfTests());
	this->setTransitions(inferedAutomata.getTransitions());
}

