#include <string>
#define StateDynamic "qs"

using namespace std;

#pragma once

/***
  Esta classe é um dos elementos básicos de um autômato juntamente com as transições.
 
 Os estados são muito importante em um autômato para se determinar um estágio e a determinada ação a entrada recebida. A classe state estrutura
 os estados de um autômato e eles podem ser nesse modelo estáticos ou dinâmicos.
 
 A classe state possui os atributos _name, symbol, activated, stateOrigin. Os atributos symbol, activated e stateOrigin são utilizados nos 
 estados dinâmicos quando o autômato é de primeira ou segunda ordem.

 Os estados estáticos são os estados tradicionais/estáticos usados nos automatos finitos determinísticos e não determinísticos, sendo necessário
 para o funcionamento do mesmo somente um nome. 
 Exemplo:
 state q1("q1);

 Os estados dinâmicos são úteis quando se é necessário referenciar um estado pela função que ele exerce e que pode sequer ainda nem existir.
 Eles são constituídos de 3 parâmetros que são os parâmetros que serão usados no método de busca Sch que está implementado na classe automaton.
 Exemplo:
 Supondo que a cadeia de entrada foi aabb e que o autômato adaptativo criou um estado chamado s4 e uma nova transição que liga o estado s4 com
 o estado q3 que é estático com o caractere a. Ao declarar um estado dinâmico com essa forma: 
 state desconhecido("q3","a",0)
 Você está dizendo procure um estado que tenha uma transição com q3 com o símbolo a e que a transição esteja partindo dele (se o terceiro parâmetro
 fosse um então estaria chegando nele).
 Importante lembrar que caso a cadeia de entrada fosse diferente, por exemplo uma cadeia maior, então o estado s4 continuaria existindo, mas poderia não ser
 o estado que conecta o estado q3 ao autômato.

*/

class state
{
public:

	bool activated, stateOrigin;
	string symbol, _name;

	state();
	state(string _name);
	state(string _name, string symbol, bool stateOrigin);

	bool operator == (const state &b) const {
		return b._name == this->_name;
	}

	bool operator < (const state &b) const {
		return this->_name < b._name;
	}

	bool operator == (string b) {
		return b == this->_name;
	}
};

