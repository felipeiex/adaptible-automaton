#pragma once
#include "negativeSequence.h"
#include "positiveSequence.h"
/***
Um aut�mato adaptativo de primeira ordem tem como dispositivo subjacente um automato finito n�o-determin�stico nesse framework, sendo assim
sequ�ncia positivas e negativas v�o adicionar e remover transi��es e estados do afnd subjacente.
A classe pairFO implementa o par ordenado adaptativo de transi��es pr�prias e estrangeiras que devem ser removidas e acrescentadas do automato.
A defini��o de transi��es pr�prias e estrangeiras est� dispon�vel no modelo te�rico na qual foi baseado o framework.
***/
class pairFO
{
public:
	positiveSequence _own;
	negativeSequence _for;

	pairFO(positiveSequence _own, negativeSequence _for);
};

