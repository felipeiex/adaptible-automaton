#include <math.h>
#include <string>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <queue>
#include <map>
#include <set>
#include "AASO.h"


int auxCont = 1;


using namespace std;

int main() {
	atribAlphabet Alfabeto;
	atribState states;
	atribTransition mapTransicoes;


	//Quantidade de simbolos no alfabeto
	int qtdAlfabeto, qtdstates, qtdTransicoes;
	cin >> qtdAlfabeto;

	//Entrada de simbolos no alfabeto
	fore(i, qtdAlfabeto)
	{
		string simbolo;
		cin >> simbolo;
		Alfabeto.insert(pair <string, int>(simbolo, i));
	}
	// Inserindo o symEmpty que ira representa o simbolo vazio
	Alfabeto.insert(pair <string, int>(symEmpty, qtdAlfabeto));
	qtdAlfabeto++;

	//Quantidade de estados
	cin >> qtdstates;

	string penultState;
	//Leitura dos estados
	fore(i, qtdstates)
	{
		string estado;
		cin >> estado;		
		state a(estado);
		if (i == qtdstates - 2) {
			penultState = estado;
		}
		pair <state, int>  b(a, i);
		states.insert(b);
	}

	//Quantidade de Transiooes
	cin >> qtdTransicoes;
	//Ler as transiooes
	fore(i, qtdTransicoes)
	{
		string origem, simbolo, destino;
		cin >> origem >> simbolo >> destino;

		state estadoOrigem, estadoDestino;
		int idOrigem = -2, idDestino = -2;
		if (Alfabeto.find(simbolo) == Alfabeto.end())  /* Caso seja o simbolo vazio */
		{
			simbolo = symEmpty;
		}
		atribState::iterator it = states.begin();
		for (it; it != states.end(); it++) {
			if (origem == it->first._name) estadoOrigem = it->first;
			if (destino == it->first._name) estadoDestino = it->first;
		}


		transition a(estadoOrigem, simbolo, estadoDestino);
		pair <int, transition> b(i, a);

		mapTransicoes.insert(b);
		//7818694 Bruno
	}

	//Quantidade de estados iniciais
	int qtdstatesIniciais;
	cin >> qtdstatesIniciais;
	atribState estadosIniciais, estadosFinais;

	//Leitura dos estados iniciais
	fore(i, qtdstatesIniciais)
	{
		string estadoInicial;
		cin >> estadoInicial;
		atribState::iterator it;
		int auxFinal = 0;
		it = states.begin();
		for (it; it != states.end(); it++) {
			if (it->first._name == estadoInicial) auxFinal = it->second;
		}
		pair <state, int> b(estadoInicial, auxFinal);
		estadosIniciais.insert(b);
	}

	//Quantidade de estados finais
	int qtdstatesFinais;
	cin >> qtdstatesFinais;

	//Leitura dos estados finais
	fore(i, qtdstatesFinais)
	{
		string estadoFinal;
		cin >> estadoFinal;
		int auxFinal;
		atribState::iterator it;
		it = states.begin();
		for (it; it != states.end(); it++) {
			if (it->first._name == estadoFinal) auxFinal = it->second;
		}

		pair <state, int> b(estadoFinal, auxFinal);
		estadosFinais.insert(b);

	}
	
	string _name;
	cin >> _name; /* Nome do Automato Finito Nao-Deterministico */
	
	atribPairFO transPro;
	atribPairFO transFor;
	positiveSequenceSO auxSO1;
	negativeSequenceSO auxSO2;
	pairSO novoSO;
	atribPairSO conjuntodeComportamentosSO;


	//map <int, transition>
	atribTransition _pro;
	atribTransition _for;

	positiveSequence transPro2;
	negativeSequence transFor2;


	atribPairFO conjuntodeComportamentosPO;
	atribPairFO auxPO;

	//F1
	_pro.insert(make_pair(2, transition(state("q2"), "a", state("q10"))));
	_pro.insert(make_pair(4, transition(state("q4"), "b", state("q10"))));
	_for.insert(make_pair(10, transition(state("q2"), "a", state("q2"))));
	_for.insert(make_pair(11, transition(state("q4"), "b", state("q7"))));
	_for.insert(make_pair(12, transition(state("q9"), "c", state("q8"))));
	transPro2 = positiveSequence(_pro);
	transFor2 = negativeSequence(_for);
	pairFO novoPO(transPro2, transFor2);
	//pairFO novoPO;
	pair < int, pairFO > parAux(2, novoPO);
	pair < int, pairFO > F1(2, novoPO);

	conjuntodeComportamentosPO.insert(parAux);


	//F2
	_pro.erase(_pro.begin(), _pro.end()); _for.erase(_for.begin(), _for.end());

	_pro.insert(make_pair(2, transition(state("q2"), "a", state("q10"))));
	_pro.insert(make_pair(4, transition(state("q4"), "b", state("q10"))));
	_for.insert(make_pair(9, transition(state("q4"), "b", state("q4"))));

	transPro2 = positiveSequence(_pro);
	transFor2 = negativeSequence(_for);
	novoPO = pairFO(transPro2, transFor2);
	//pairFO novoPO;
	parAux = pair <int, pairFO>(4, novoPO);
	pair < int, pairFO > F2(4, novoPO);
	conjuntodeComportamentosPO.insert(parAux);
	auxPO.insert(parAux);


	//F3
	_pro.erase(_pro.begin(), _pro.end()); _for.erase(_for.begin(), _for.end());

	_pro.insert(make_pair(11, transition(state("q4"), "b", state("q7"))));
	_pro.insert(make_pair(12, transition(state("q9"), "c", state("q8"))));

	transPro2 = positiveSequence(_pro);
	transFor2 = negativeSequence(_for);
	novoPO = pairFO(transPro2, transFor2);

	//pairFO novoPO;
	parAux = pair <int, pairFO>(8, novoPO);
	pair < int, pairFO > F3(8, novoPO);
	conjuntodeComportamentosPO.insert(parAux);
	auxPO.insert(parAux);


	//F4
	_pro.erase(_pro.begin(), _pro.end()); _for.erase(_for.begin(), _for.end());

	_pro.insert(make_pair(_pro.size(), transition(state("q4"), "b", state("q7"))));
	_pro.insert(make_pair(_pro.size(), transition(state("q8"), "c", state("q8"))));


	transPro2 = positiveSequence(_pro);
	transFor2 = negativeSequence(_for);
	novoPO = pairFO(transPro2, transFor2);
	//pairFO novoPO;
	parAux = pair <int, pairFO>(6, novoPO);
	pair < int, pairFO > F4(6, novoPO);
	conjuntodeComportamentosPO.insert(parAux);
	auxPO.insert(parAux);


	//F5
	_pro.erase(_pro.begin(), _pro.end());
	_for.erase(_for.begin(), _for.end());


	_pro.insert(make_pair(_pro.size(), transition(state("q4", "b", 0), "b", state("q4"))));
	_for.insert(make_pair(_for.size(), transition(state("q4", "b", 0), "b", state("qs"))));
	_for.insert(make_pair(_for.size(), transition(state("qs"), "b", state("q4"))));

	transPro2 = positiveSequence(_pro);
	transFor2 = negativeSequence(_for);

	novoPO = pairFO(transPro2, transFor2);
	//pairFO novoPO;
	parAux = pair <int, pairFO>(10, novoPO);
	pair < int, pairFO > F5(10, novoPO);

	//F6
	_pro.erase(_pro.begin(), _pro.end()); _for.erase(_for.begin(), _for.end());


	_pro.insert(make_pair(_pro.size(), transition(state("q9", "c", 0), "c", state("q9"))));
	_for.insert(make_pair(_for.size(), transition(state("q9", "c", 0), "c", state("qs"))));
	_for.insert(make_pair(_for.size(), transition(state("qs"), "c", state("q9"))));

	transPro2 = positiveSequence(_pro);
	transFor2 = negativeSequence(_for);
	novoPO = pairFO(transPro2, transFor2);
	//pairFO novoPO;
	parAux = pair <int, pairFO>(10, novoPO);
	pair < int, pairFO > F6(10, novoPO);
	auxPO.insert(parAux);


	//F7
	_pro.erase(_pro.begin(), _pro.end()); _for.erase(_for.begin(), _for.end());


	_pro.insert(make_pair(_pro.size(), transition(state("q9", "c", 0), "c", state("q9"))));
	_for.insert(make_pair(_for.size(), transition(state("q9", "c", 0), "c", state("qs"))));
	_for.insert(make_pair(_for.size(), transition(state("qs"), "c", state("q9"))));

	transPro2 = positiveSequence(_pro);
	transFor2 = negativeSequence(_for);
	novoPO = pairFO(transPro2, transFor2);
	//pairFO novoPO;
	parAux = pair <int, pairFO>(9, novoPO);
	pair < int, pairFO > F7(9, novoPO);
	auxPO.insert(parAux);


	//typedef map <int, pairFO> atribPairFO;

	atribPairSO mapSO;

	pairSO parAuxSO;
	negativeSequenceSO auxNegativaSO;
	positiveSequenceSO auxPositivoSO;
	atribPairFO auxParesSONeg;
	atribPairFO auxParesSOPos;


	//G1
	auxParesSOPos.erase(auxParesSOPos.begin(), auxParesSOPos.end());
	auxParesSONeg.erase(auxParesSONeg.begin(), auxParesSONeg.end());

	auxParesSONeg.insert(F7);

	auxPositivoSO = positiveSequenceSO(auxParesSOPos);
	auxNegativaSO = negativeSequenceSO(auxParesSONeg);


	parAuxSO = pairSO(auxPositivoSO, auxNegativaSO);
	mapSO.insert(make_pair(4, parAuxSO));


	//G2
	auxParesSOPos.erase(auxParesSOPos.begin(), auxParesSOPos.end());
	auxParesSONeg.erase(auxParesSONeg.begin(), auxParesSONeg.end());

	auxParesSONeg.insert(F5);

	auxPositivoSO = positiveSequenceSO(auxParesSOPos);
	auxNegativaSO = negativeSequenceSO(auxParesSONeg);


	parAuxSO = pairSO(auxPositivoSO, auxNegativaSO);
	mapSO.insert(make_pair(8, parAuxSO));


	//G3
	auxParesSOPos.erase(auxParesSOPos.begin(), auxParesSOPos.end());
	auxParesSONeg.erase(auxParesSONeg.begin(), auxParesSONeg.end());

	auxParesSONeg.insert(F6);

	auxPositivoSO = positiveSequenceSO(auxParesSOPos);
	auxNegativaSO = negativeSequenceSO(auxParesSONeg);


	parAuxSO = pairSO(auxPositivoSO, auxNegativaSO);
	mapSO.insert(make_pair(6, parAuxSO));

	AASO novo(_name, Alfabeto, states, estadosIniciais, estadosFinais, mapTransicoes, conjuntodeComportamentosPO, mapSO);
	//AASO novo(nome, Alfabeto, states,estadosIniciais, estadosFinais, mapTransicoes);
	string auxEntrada;
	cout << "Etapa de treinamento iniciada" << endl;

	while (cin >> auxEntrada) {		
		if (novo.flag) {
			//novo.inferedAutomata = novo;
			break;
		}
		if (novo.inferAutomata(auxEntrada)) cout << "Aceito" << endl;
		else cout << "Nao aceito" << endl;
	}

	cout << "Etapa de treinamento finalizada" << endl;
	cout << "TREINADO :::::: " << endl;
	novo.printTransitions();
	novo.printSetOfTests();
	while (cin >> auxEntrada) {
		if (novo.inferedAutomata.output(auxEntrada)) cout << "Aceito" << endl;
		else cout << "Nao aceito" << endl;
	}
	
}