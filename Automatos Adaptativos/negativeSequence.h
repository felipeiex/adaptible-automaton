#pragma once
#include "automaton.h"
/***
Sequ�ncias negativas s�o transi��es estrangeiras que devem ser acrescentadas ao automato. Aqui representada pela classe negativeSequence.
***/
class negativeSequence
{
public:
	atribTransition foreignTransition;
	negativeSequence(atribTransition foreignTransition);
	negativeSequence() {}

};

