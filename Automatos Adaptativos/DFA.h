#pragma once
#include "automaton.h"
/***
A classe DFA abreviação de Deterministic Finit Automata (Autômato Finito Determinístico) possui o método output implementado que permite saber se uma 
palavra pertence ou não ao autômato. Se o retorno do método output for verdadeiro então significa que a palavra parametrizada pertence ao autômato, se for falsa
significa que não pertence ao autômato.
*/


class DFA :
	public automaton
{
public:	
	DFA(string _name, atribAlphabet alphabet, atribState _state, atribState _initialStates, atribState _finalStates,
		atribTransition _transitions) :automaton(_name, alphabet, _state, _initialStates, _finalStates, _transitions) {
		cout << mCreateAFD << endl;
	}

	bool output(string word, state current);
	bool output(string word);
	
};

